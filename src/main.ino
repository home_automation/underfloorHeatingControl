#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <wifi_auth.h>

#define OFF 1
#define ON  0
#define LIVING_ROOM_GPIO    D5
#define BATH_ROOM_GPIO      D6
#define CHILDREN_ROOM_GPIO  D7
#define SLEEPING_ROOM_GPIO  D8

ESP8266WebServer server(80);

void handleRoot() {
  createHp();
}

void handleCtrl() {
  uint8_t room_gpio = getGpioFromRoom(server.arg("room"));
  uint8_t room_doCmd = getDoCmdFromRoom(server.arg("switch"));
  digitalWrite(room_gpio, room_doCmd);
  createHp();
}

uint8_t getGpioFromRoom(String room) {
  if (room == "lr") {
    return (uint8_t)LIVING_ROOM_GPIO;
  }
  if (room == "br") {
    return (uint8_t)BATH_ROOM_GPIO;
  }
  if (room == "cr") {
    return (uint8_t)CHILDREN_ROOM_GPIO;
  }
  if (room == "sr") {
    return (uint8_t)SLEEPING_ROOM_GPIO;
  }
  return (uint8_t)LED_BUILTIN;
}

uint8_t getDoCmdFromRoom(String doCmd) {
  if (doCmd == "on") {
    return (uint8_t)ON;
  }
  return (uint8_t)OFF;
}

void handleNotFound(){
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void createHp() {
  String message = "<h1>Night mode underfloor heating</h1>";
  message += "<p>";
  message += createHp_button("lr");
  message += "</p>";
  message += "<p>";
  message += createHp_button("br");
  message += "</p>";
  message += "<p>";
  message += createHp_button("cr");
  message += "</p>";
  message += "<p>";
  message += createHp_button("sr");
  message += "</p>";
  message += "<p>";
  message += createHp_button("led");
  message += "</p>";
  server.send(200, "text/html", message);
}

String createHp_title() {
  return "<h1>Night mode underfloor heating</h1>";
}

String getButtRef(String room) {
  uint8_t gpio = getGpioFromRoom(room);
  String ref = "\"ctrl?room=";
  ref += room;
  if (digitalRead(gpio)) {
    return ref + "&switch=on\"";
  }
  return ref + "&switch=off\"";
}

String getButtColor(String room) {
  uint8_t gpio = getGpioFromRoom(room);
  if (digitalRead(gpio)) {
    return "white";
  }
  return "LawnGreen";
}

String getButtText(String room) {
  if (room == "lr") {
    return "Living room";
  }
  if (room == "br") {
    return "Bath room";
  }
  if (room == "cr") {
    return "Children room";
  }
  if (room == "sr") {
    return "Sleeping room";
  }
  return "Internal LED";
}

String createHp_button(String room) {
  String ref = getButtRef(room);
  String col = getButtColor(room);
  String txt = getButtText(room);
  String html = "<svg width=\"400\" height=\"100\">";
  html += "<a xlink:href=";
  html += ref;
  html += " xlink:type=\"simple\">";
  html += "<rect x=\"0\" y=\"0\" width=\"400\" height=\"100\" stroke=\"black\" stroke-width=\"3px\" fill=\"";
  html += col;
  html += "\"/>";
  html += "<text font-size=\"30px\" font-family=\"courier,sans-serif\" font-weight=\"bold\" x=\"50%\" y=\"50%\" dominant-baseline=\"middle\" text-anchor=\"middle\">";
  html += txt;
  html += "</text>";
  html += "</a>";
  html += "</svg>";
  return html;
}

void setupGPIOS() {
  pinMode(LIVING_ROOM_GPIO, OUTPUT);
  pinMode(BATH_ROOM_GPIO, OUTPUT);
  pinMode(CHILDREN_ROOM_GPIO, OUTPUT);
  pinMode(SLEEPING_ROOM_GPIO, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  digitalWrite(LIVING_ROOM_GPIO, OFF);
  digitalWrite(BATH_ROOM_GPIO, OFF);
  digitalWrite(CHILDREN_ROOM_GPIO, OFF);
  digitalWrite(SLEEPING_ROOM_GPIO, OFF);
  digitalWrite(LED_BUILTIN, ON);
}

void setup(void){
  setupGPIOS();
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);
  server.on("/ctrl", handleCtrl);

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  server.handleClient();
}
